package pricewatcher.hw0;

import java.io.PrintStream;
import java.util.Random;

public class UI {
    private PrintStream out;

    private Item item;
    private String name;
    Random rand = new Random();
    public UI(){
        this(System.out);
    }

    public UI(PrintStream out){
        this.out = out;
        //this.item = item;
    }

    public void showMessage(String s) {
        out.println(s);
    }

    public void showItem() {
        out.println("Name:"+getName());
        out.println("URL:"+getURL());
        out.println("Price:"+getPrice());
        out.println("Change:"+getChange());
        out.println("Added:"+getAdded());
        out.println("Enter 1 to Check Price " +
                "Enter 2 to View Page" +
                " or enter -1 to Quit");
    }
    public void showUpdatedItem() {
        out.println("Name:"+getName());
        out.println("URL:"+getURL());
        out.println("Price:"+getNewPrice());
        out.println("Change:"+getNewChange());
        out.println("Added:"+getAdded());
        out.println("Enter 1 to Check Price " +
                "Enter 2 to View Page" +
                " or enter -1 to Quit");
    }
    public int viewPage(){
        //1000 Auto-generated method stub
        out.println("Click the URL:"+getURL());
        out.println("Enter 1 to Check Price " +
                "Enter 2 to View Page" +
                " or enter -1 to Quit");
        return -1;
    }

    public String getName() {
        String name = "MacBook Pro 2018 15";
        return name;
    }
    public String getURL() {
        String URL = "https://www.apple.com/shop/buy-mac/macbook-pro/15-inch-space-gray-2.6ghz-6-core-512gb#";
        return URL;
    }
    public String getPrice() {
        String Price = "$2,799.00";
        return Price;
    }
    public String getChange() {
        String Change = "00.00%";
        return Change;
    }
    public String getAdded() {
        String Added = "09/03/2018 ($2,799.00)";
        return Added;
    }

    int randomnumber = rand.nextInt(2798);

    public String getNewPrice() {
        String Price = randomnumber+"$";
        return Price;
    }
    public String getNewChange() {
        double percentage = (2799.0-randomnumber)/2799.0;
        double percentageP = percentage*100.0;
        String Change = percentageP+"%";
        return Change;
    }
    //Added 09/06
    /*
    public int readSelection() {
        while (true) {
            out.print("\n\nEnter 1 (to check price), 2 (to view page), or -1 to quit? ");
            out.flush();

            // read a valid selection and return it.
        }
    }


    public enum Selection {
        PRICE(1, new PriceHandler()),
        VIEW(2, new ViewHandler()),
        QUIT(-1, null);

        public final int number;
        public final SelectionHandler handler;

        Selection(int number, SelectionHandler handler) {
            this.number = number;
            this.handler = handler;
        }
    }
    **/
}