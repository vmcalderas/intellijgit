package pricewatcher.hw0;
import java.util.Scanner;

public class Main {
    private UI ui;
    private Item item;

    public Main(){
        item = new Item();
        ui = new UI();
    }

    public void run(){
        // show welcome messsage
        ui.showMessage("Welcome to price watcher!");

        //Show print item
        ui.showItem();

        //repeat until user quit
        Scanner userInput = new Scanner(System.in);
        //get user selection
        //process user selection
        int input = userInput.nextInt();
        while(input!= -1){
            if(input == 1){
                //Price Check
                ui.showUpdatedItem();
            }
            if(input == 2){
                //View Page
                ui.viewPage();
            }
            input = userInput.nextInt();
        }


        // Close the scanner object using close() method to prevent memory leak
        userInput.close();
        //print bye message
        ui.showMessage("Thank you for visiting!");

        //Added 09/06
        /*
        ui.Selection sel = ui.readSelection();
        while(sel != UI.Selection.QUIT) {
            for (UI.Selection s: ui.Selection.values()) {
                if (sel == s) {
                    s.handler.handle();
                    break;
                }
            }
        }
        **/

    }


}
